"use strict";
function showHostName() {
	var message = "Page location is " + window.location.href;
	message += "<br>" + "Page hostname is: " + window.location.hostname;
	message += "<br>" + "cookiesEnabled is " + navigator.cookieEnabled;
	message += "<br>" + "navigator.appName is " + navigator.appName;
	message += "<br>" + "navigator.appCodeName is " + navigator.appCodeName;
	message += "<br>" + "navigator.product is " + navigator.product + " Version: " + navigator.appVersion;
	document.getElementById("browserInfo").innerHTML = message;
}
function changeTextDone(id) {
    id.innerHTML = "Thank you, changed!";
}
function popupInput() {
	var txt;
	var person = prompt("Please enter your name:", "Harry Potter");
	if (person == null || person == "") {
	  txt = "User cancelled the prompt.";
	} else {
	  txt = "Hello " + person + "! How are you today?";
	}
	document.getElementById("popupMessage").innerHTML = txt;
}
function showAlert() {
	alert('Hello');
}
function loadAJAXContent() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
		document.getElementById("contentArea").innerHTML =
		this.responseText;
		}
	};
	xhttp.open("GET", "../XML/ajax_info.txt", true);
	xhttp.send();
}
function loadCDList() {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
	  if (this.readyState == 4 && this.status == 200) {
		fillCDList(this);
	  }
	};
	xhttp.open("GET", "../XML/cd_catalog.xml", true);
	xhttp.send();
  }
function fillCDList(xml) {
	var i;
	var xmlDoc = xml.responseXML;
	var table="<tr><th>Artist</th><th>Title</th></tr>";
	var x = xmlDoc.getElementsByTagName("CD");
	for (i = 0; i <x.length; i++) { 
	  table += "<tr><td>" +
	  x[i].getElementsByTagName("ARTIST")[0].childNodes[0].nodeValue +
	  "</td><td>" +
	  x[i].getElementsByTagName("TITLE")[0].childNodes[0].nodeValue +
	  "</td></tr>";
	}
	document.getElementById("cdList").innerHTML = table;
  }