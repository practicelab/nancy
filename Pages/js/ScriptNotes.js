"use strict";
var points = [40, 100, 1, 5, 25, 10];
var person = {
  firstName: "John",
  lastName : "Doe",
  id     : 5566,
  fullName : function() {
    return this.firstName + " " + this.lastName;
  }
};
var employeeList = '{"employees":[' +
'{"firstName":"John","lastName":"Doe" },' +
'{"firstName":"Anna","lastName":"Smith" },' +
'{"firstName":"Peter","lastName":"Jones" }]}';

var xmlhttp = new XMLHttpRequest();

xmlhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    myObj = JSON.parse(this.responseText);
    document.getElementById("demoPHP").innerHTML = myObj.name;
  }
};
xmlhttp.open("GET", "demo_file.php", true);
xmlhttp.send();

var hello;

hello = () => {
	document.getElementById("messageDisplay").innerHTML += this;
	var obj = JSON.parse(employeeList);
	document.getElementById("employeeNames").innerHTML =
	obj.employees.toString();
    //obj.employees[0].firstName + " " + obj.employees[0].lastName;
}

//The window object calls the function:
window.addEventListener("load", hello);
document.getElementById("showDateTime").addEventListener("click", displayDate);

//A button object calls the function:
document.getElementById("demoArrowFunction").addEventListener("click", hello);

document.getElementById("arrayDisplay").innerHTML = "The original array: " + points;
document.getElementById("personFullName").innerHTML = person.fullName();

function sortArrayAscending() {
  points.sort(function(a, b){return a - b});
  displaySortedArray();
  //document.getElementById("sortedArrayDisplay").innerHTML = "The sorted array: " + points;
}

function sortArrayDescending() {
  points.sort(function(a, b){return b - a});
  displaySortedArray();
  //document.getElementById("sortedArrayDisplay").innerHTML = "The sorted array: " + points;
}
function displaySortedArray() {
  document.getElementById("sortedArrayDisplay").innerHTML = "The sorted array: " + points; 
}
function arrayCalculation() {
	var result, totalValue;
	points.sort(function(a, b){return a-b});
	result = "min=" + points[0];
	result += " max=" + points[points.length -1];
	totalValue = points.reduce(function(total, value){return total+value});
	result += " total=" + totalValue;
	result += " mean=" + totalValue/points.length;
	document.getElementById("resultDisplay").innerHTML = result;    
}
function myArrayMax(arr) {
	var len = arr.length;
	var max = -Infinity;
	while (len--) {
			if (arr[len] > max) {
			max = arr[len];
			}
	}
	return max;
}
function myArrayMin(arr) {
	var len = arr.length;
	var min = Infinity;
	while (len--) {
		if (arr[len] < min) {
			min = arr[len];
		}
	}
	return min;
}
function validateInput() {
  var message, x;
  message = document.getElementById("pInput");
  message.innerHTML = "";
  x = document.getElementById("inputValue").value;
  try {
    if(x == "") throw "empty";
    if(isNaN(x)) throw "not a number";
    x = Number(x);
    if(x < 5) throw "too low";
    if(x > 10) throw "too high";
  }
  catch(err) {
    message.innerHTML = "Input is " + err;
  }
}
function numberRangeCheck() {
  var inpObj = document.getElementById("numInput");
  if (!inpObj.checkValidity()) {
    document.getElementById("messageCheck").innerHTML = inpObj.validationMessage;
  } else {
    document.getElementById("messageCheck").innerHTML = "Input OK";
  }
}
function sumAll() {
  var i;
  var sum = 0;
  for (i = 0; i < arguments.length; i++) {
    sum += arguments[i];
  }
  return sum;
}
function findMax() {
  var i;
  var max = -Infinity;
  for (i = 0; i < arguments.length; i++) {
    if (arguments[i] > max) {
      max = arguments[i];
    }
  }
  return max;
}
function displayMax() {
	document.getElementById("displayFunctionResult").innerHTML = "Max=" + findMax(15, 24, 2, 56, 78);
}
function displaySum() {
	document.getElementById("displayFunctionResult").innerHTML = "Sum=" + sumAll(15, 24, 2, 56, 78);
}
function displayMean() {
	document.getElementById("displayFunctionResult").innerHTML = "Mean=" + sumAll(15, 24, 2, 56, 78)/5;
}
var increaseCounter = (function () {
  var counter = 0;
  return function () {counter += 1; return counter;}
})();

function showCounter(){
  document.getElementById("displayCounter").innerHTML = increaseCounter();
}
function animationMove() {
  var elem = document.getElementById("animate");
  var pos = 0;
  var id = setInterval(frame, 5);
  function frame() {
    if (pos == 350) {
      clearInterval(id);
    } else {
      pos++;
      elem.style.top = pos + 'px';
      elem.style.left = pos + 'px';
    }
  }
}
function changeText(id) {
  id.innerHTML = "Ooops!";
}
function upperCase() {
  var x = document.getElementById("inputString");
  x.value = x.value.toUpperCase();
}
function mDown(obj) {
  obj.style.backgroundColor = "#1ec5e5";
  obj.innerHTML = "Release Me";
}

function mUp(obj) {
  obj.style.backgroundColor="#D94A38";
  obj.innerHTML="Thank You";
}
function displayDate() {
	document.getElementById("displayDate").innerHTML = Date();
}
